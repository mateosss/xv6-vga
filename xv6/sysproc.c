#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int
sys_modeswitch(void)
{
  int mode;
  if(argint(0, &mode) < 0)
    return -1;
  set_graphics_mode(mode);
  return 0;
}

int
sys_plotpixel(void)
{
  int x, y, color;
  if(argint(0, &x) < 0 || argint(1, &y) < 0 || argint(2, &color) < 0)
    return -1;
  drawpixel(x, y, color);
  return 0;
}

int
sys_animate(void)
{
  int x, y, height, width, length, dir;
  char *frames;
  if(argint(0, &x) < 0 || argint(1, &y) < 0 || argint(2, &height) < 0 ||
     argint(3, &width) < 0 || argint(5, &length) < 0 || argint(6, &dir) < 0 || 
     argptr(4, &frames, 0) < 0) // REVIEW: third argument should be width * height * length but doesn't work
    return -1;
  vga_animate(x, y, height, width, (char **) frames, length, dir);
  return 0;
}
int
sys_drawrect(void)
{
  int x, y, height, width, color;
  if(argint(0, &x) < 0 || argint(1, &y) < 0 || argint(2, &height) < 0 || argint(4, &width) < 0 || argint(5, &color) < 0)
    return -1;
  vga_drawrect(x, y, height, width, color);
  return 0;
}
