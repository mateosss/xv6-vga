#include "types.h"
#include "x86.h"
#include "memlayout.h"
#include "types.h"
#include "user.h"

#define	VGA_MISC_WRITE		0x3C2
#define VGA_SEQ_INDEX		0x3C4
#define VGA_SEQ_DATA		0x3C5
#define VGA_GC_INDEX 		0x3CE
#define VGA_GC_DATA 		0x3CF
/*			COLOR emulation		MONO emulation */
#define VGA_CRTC_INDEX		0x3D4		/* 0x3B4 */
#define VGA_CRTC_DATA		0x3D5		/* 0x3B5 */

#define	VGA_NUM_CRTC_REGS	25
#define	VGA_NUM_SEQ_REGS	5
#define	VGA_NUM_GC_REGS		9
#define	VGA_NUM_AC_REGS		21
#define	VGA_NUM_REGS		(1 + VGA_NUM_SEQ_REGS + VGA_NUM_CRTC_REGS + \
        VGA_NUM_GC_REGS + VGA_NUM_AC_REGS)

#define CONSOLE_BASE (char *) P2V(0xB8000)
#define VGA_BASE (char *) P2V(0xA0000)
#define BACKGROUND_COLOR 233 // 233 is blue gray 900 of material design

unsigned char g_8x16_font[4096];

unsigned char g_80x25_text[] =
{
  /* MISC */
  0x67,
  /* SEQ */
  0x03, 0x00, 0x03, 0x00, 0x02,
  /* CRTC */
  0x5F, 0x4F, 0x50, 0x82, 0x55, 0x81, 0xBF, 0x1F,
  0x00, 0x4F, 0x0D, 0x0E, 0x00, 0x00, 0x00, 0x50,
  0x9C, 0x0E, 0x8F, 0x28, 0x1F, 0x96, 0xB9, 0xA3,
  0xFF,
  /* GC */
  0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x0E, 0x00,
  0xFF,
  /* AC */
  0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x14, 0x07,
  0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F,
  0x0C, 0x00, 0x0F, 0x08, 0x00
};


unsigned char g_320x200x256[] =
{
  /* MISC */
  0x63,
  /* SEQ */
  0x03, 0x01, 0x0F, 0x00, 0x0E,
  /* CRTC */
  0x5F, 0x4F, 0x50, 0x82, 0x54, 0x80, 0xBF, 0x1F,
  0x00, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x9C, 0x0E, 0x8F, 0x28,	0x40, 0x96, 0xB9, 0xA3,
  0xFF,
  /* GC */
  0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x0F,
  0xFF,
  /* AC */
  0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
  0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
  0x41, 0x00, 0x0F, 0x00,	0x00
};

void
drawpixel(int x, int y, int color)
{
  char *base = VGA_BASE;
  int offset = 320 * y + x;
  if ((unsigned char) color == 255) {
    base[offset] = (char) BACKGROUND_COLOR;
  } else {
    base[offset] = (char) color;
  }
}

static void
vga_delay(long long int x)
{
  unsigned volatile int s = 0;
  for (long long int l = 0; l < x; l++) {
    s++;
  }
}

void
vga_drawrect(int x, int y, int height, int width, int color)
{
  for (int i = x; i < x + width; i++) {
    for (int j = y; j < y + height; j++) {
      drawpixel(j, i, color);
    }
  }
}

void
vga_animate(int x, int y, int height, int width, char **frames, int length, int dir)
{
  int maxk, dirx, diry;
  maxk = dir % 2 ? 120 : 75;
  if (dir == 0) {
    dirx = 0;
    diry = -3;
  } else if (dir == 1) {
    dirx = 3;
    diry = 0;
  } else if (dir == 2) {
    dirx = 0;
    diry = 3;
  } else {
    dirx = -3;
    diry = 0;
  }
  for (int k = 0; k < maxk; k++) {
    if ((dir == 0 && y > 4) || (dir == 1 && x < 292/2 - 12) ||
        (dir == 2 && y < 168/2 - 18) || (dir == 3 && x > 0))
    {
      for (int i = y; i < y + width; i++) {
        for (int j = x; j < x + height; j++) { // Draw a 2x2 square for each pixel to scale the sprite
          drawpixel(j * 2, i * 2, frames[k % length][(i - y)*height + (j - x)]);
          drawpixel(j * 2 + 1, i * 2, frames[k % length][(i - y)*height + (j - x)]);
          drawpixel(j * 2, i * 2 + 1, frames[k % length][(i - y)*height + (j - x)]);
          drawpixel(j * 2 + 1 , i * 2 + 1, frames[k % length][(i - y)*height + (j - x)]);
        }
      }
      vga_delay(2500000);
      vga_drawrect(y * 2, x * 2, 50, 64, BACKGROUND_COLOR);
      x = x + dirx;
      y = y + diry;
    }
  }
}

static void
initialize_palette(void)
{
  // Material design palette
  int vga256_24bit[256] = {
    // amber
    0xfff8e1, 0xffecb3, 0xffe082, 0xffd54f, 0xffca28, 0xffc107, 0xffb300,
    0xffa000, 0xff8f00, 0xff6f00, 0xffe57f, 0xffd740, 0xffc400, 0xffab00,
    // blue
    0xe3f2fd, 0xbbdefb, 0x90caf9, 0x64b5f6, 0x42a5f5, 0x2196f3, 0x1e88e5,
    0x1976d2, 0x1565c0, 0x0d47a1, 0x82b1ff, 0x448aff, 0x2979ff, 0x2962ff,
    // cyan
    0xe0f7fa, 0xb2ebf2, 0x80deea, 0x4dd0e1, 0x26c6da, 0x00bcd4, 0x00acc1,
    0x0097a7, 0x00838f, 0x006064, 0x84ffff, 0x18ffff, 0x00e5ff, 0x00b8d4,
    // deep-orange
    0xfbe9e7, 0xffccbc, 0xffab91, 0xff8a65, 0xff7043, 0xff5722, 0xf4511e,
    0xe64a19, 0xd84315, 0xbf360c, 0xff9e80, 0xff6e40, 0xff3d00, 0xdd2c00,
    // deep-purple
    0xede7f6, 0xd1c4e9, 0xb39ddb, 0x9575cd, 0x7e57c2, 0x673ab7, 0x5e35b1,
    0x512da8, 0x4527a0, 0x311b92, 0xb388ff, 0x7c4dff, 0x651fff, 0x6200ea,
    // green
    0xe8f5e9, 0xc8e6c9, 0xa5d6a7, 0x81c784, 0x66bb6a, 0x4caf50, 0x43a047,
    0x388e3c, 0x2e7d32, 0x1b5e20, 0xb9f6ca, 0x69f0ae, 0x00e676, 0x00c853,
    // indigo
    0xe8eaf6, 0xc5cae9, 0x9fa8da, 0x7986cb, 0x5c6bc0, 0x3f51b5, 0x3949ab,
    0x303f9f, 0x283593, 0x1a237e, 0x8c9eff, 0x536dfe, 0x3d5afe, 0x304ffe,
    // light-blue
    0xe1f5fe, 0xb3e5fc, 0x81d4fa, 0x4fc3f7, 0x29b6f6, 0x03a9f4, 0x039be5,
    0x0288d1, 0x0277bd, 0x01579b, 0x80d8ff, 0x40c4ff, 0x00b0ff, 0x0091ea,
    // light-green
    0xf1f8e9, 0xdcedc8, 0xc5e1a5, 0xaed581, 0x9ccc65, 0x8bc34a, 0x7cb342,
    0x689f38, 0x558b2f, 0x33691e, 0xccff90, 0xb2ff59, 0x76ff03, 0x64dd17,
    // lime
    0xf9fbe7, 0xf0f4c3, 0xe6ee9c, 0xdce775, 0xd4e157, 0xcddc39, 0xc0ca33,
    0xafb42b, 0x9e9d24, 0x827717, 0xf4ff81, 0xeeff41, 0xc6ff00, 0xaeea00,
    // orange
    0xfff3e0, 0xffe0b2, 0xffcc80, 0xffb74d, 0xffa726, 0xff9800, 0xfb8c00,
    0xf57c00, 0xef6c00, 0xe65100, 0xffd180, 0xffab40, 0xff9100, 0xff6d00,
    // pink
    0xfce4ec, 0xf8bbd0, 0xf48fb1, 0xf06292, 0xec407a, 0xe91e63, 0xd81b60,
    0xc2185b, 0xad1457, 0x880e4f, 0xff80ab, 0xff4081, 0xf50057, 0xc51162,
    // purple
    0xf3e5f5, 0xe1bee7, 0xce93d8, 0xba68c8, 0xab47bc, 0x9c27b0, 0x8e24aa,
    0x7b1fa2, 0x6a1b9a, 0x4a148c, 0xea80fc, 0xe040fb, 0xd500f9, 0xaa00ff,
    // red
    0xffebee, 0xffcdd2, 0xef9a9a, 0xe57373, 0xef5350, 0xf44336, 0xe53935,
    0xd32f2f, 0xc62828, 0xb71c1c, 0xff8a80, 0xff5252, 0xff1744, 0xd50000,
    // teal
    0xe0f2f1, 0xb2dfdb, 0x80cbc4, 0x4db6ac, 0x26a69a, 0x009688, 0x00897b,
    0x00796b, 0x00695c, 0x004d40, 0xa7ffeb, 0x64ffda, 0x1de9b6, 0x00bfa5,
    // yellow
    0xfffde7, 0xfff9c4, 0xfff59d, 0xfff176, 0xffee58, 0xffeb3b, 0xfdd835,
    0xfbc02d, 0xf9a825, 0xf57f17, 0xffff8d, 0xffff00, 0xffea00, 0xffd600,
    // blue-grey
    0xeceff1, 0xcfd8dc, 0xb0bec5, 0x90a4ae, 0x78909c, 0x607d8b, 0x546e7a,
    0x455a64, 0x37474f, 0x263238,
    // brown
    0xefebe9, 0xd7ccc8, 0xbcaaa4, 0xa1887f, 0x8d6e63, 0x795548, 0x6d4c41,
    0x5d4037, 0x4e342e, 0x3e2723,
    // grey
    0xfafafa, 0xf5f5f5, 0xeeeeee, 0xe0e0e0, 0xbdbdbd, 0x9e9e9e, 0x757575,
    0x616161, 0x424242, 0x212121,
    // black and white
    0x000000, 0xffffff
  };

  for(int i = 0; i < 256; i++) {
    int value = vga256_24bit[i];
    outb(0x3C8, i);
    outb(0x3C9, (value >> 18) & 0x3f);
    outb(0x3C9, (value >> 10) & 0x3f);
    outb(0x3C9, (value >> 2) & 0x3f);
  }
}


static void
writei(ushort port, uchar index, uchar value)
{
  outb(port, index);
  outb(port + 1, value);
}

static void
select_fontmem(void)
{
  writei(VGA_SEQ_INDEX, 0x02, 0x04); // set write enable to plane 2 in the sequencer (this plain contains the fonts)
  writei(VGA_GC_INDEX, 0x04, 0x02); // read map select: plane 2 (the font lies here)
  writei(VGA_GC_INDEX, 0x05, 0x00); // disable odd/even addresses and set read/write mode 0
  writei(VGA_GC_INDEX, 0x06, 0x00); // map plane 2 to 0xa0000/VGA_BASE (128k size) cpu address
}

static void
unselect_fontmem(void)
{
  writei(VGA_SEQ_INDEX, 0x02, 0x03); // restore write enable to planes 0 and 1 only
  writei(VGA_GC_INDEX, 0x04, 0x00); // read map select: plane 0
  writei(VGA_GC_INDEX, 0x05, 0x10); // odd/even enable
  writei(VGA_GC_INDEX, 0x06, 0x0E); // map plane 0 memory back to 0xb8000/CONSOLE_BASE
}

static void
set_cursorpos(uchar pos)
{
    writei(VGA_CRTC_INDEX, 14, pos);
    writei(VGA_CRTC_INDEX, 15, pos);
}

void
set_graphics_mode(int mode)
{
	unsigned i;
  unsigned char *regs = (unsigned char *) (mode ? &g_320x200x256 : &g_80x25_text);

  /* write MISCELLANEOUS reg */
  outb(VGA_MISC_WRITE, *regs);
  regs++;

  /* write SEQUENCER regs */
  for(i = 00; i < VGA_NUM_SEQ_REGS; i++)
  {
    outb(VGA_SEQ_INDEX, i);
    outb(VGA_SEQ_DATA, *regs);
    regs++;
  }

  /* write CRTC regs */
  for(i = 00; i < VGA_NUM_CRTC_REGS; i++)
  {
    if (i != 14 && i != 15) { // ignore cursor related CRTC ports 14 and 15
      outb(VGA_CRTC_INDEX, i);
      outb(VGA_CRTC_DATA, *regs);
    }
    regs++;
  }

  /* write GRAPHICS CONTROLLER regs */
  for(i = 00; i < VGA_NUM_GC_REGS; i++)
  {
    outb(VGA_GC_INDEX, i);
    outb(VGA_GC_DATA, *regs);
    regs++;
  }

  if (mode) {
    // Fillscreen with color
    vga_drawrect(0, 0, 320, 200, BACKGROUND_COLOR);
  } else {
    // Restore font
    select_fontmem();
    for(int i = 0; i < 4096; i += 16) {
      for(int j = 0; j < 16; j++) {
        (VGA_BASE)[2 * i + j] = g_8x16_font[i + j];
      }
    }
    unselect_fontmem();

    // Clean screen and reset cursor position
    for (int i = 0; i < 80 * 25 * 2; i++) {
      (CONSOLE_BASE)[i] = 0;
    }
    set_cursorpos(0);

    // Set SO2018 text
    ushort *base = (ushort *) CONSOLE_BASE;
    int offset = 80 * 23 + 72;
    char *j = (char[]) {0xf0, 0xf0, 0x70, 0x70, 0x70, 0x70};
    for (char *i = "SO2018"; *i; offset++) {
      base[offset] = *(j++) << 8 | *(i++);
    }
  }

}

void
vgainit(int mode)
{
  // Save font for later
  select_fontmem();
  for(int i = 0; i < 4096; i += 16) {
    for(int j = 0; j < 16; j++) {
      g_8x16_font[i + j] = (VGA_BASE)[2 * i + j];
    }
  }
  unselect_fontmem();

  // Set material design palette
  initialize_palette();

  // Set proper registers
  set_graphics_mode(mode);
}
