#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  int x = atoi(argv[1]);
  int y = atoi(argv[2]);
  int color = atoi(argv[3]);
  plotpixel(x, y, color);
  exit();
}
