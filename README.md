# Como probar la demo?

Hay dos user programs para testear esto:

- `vgatest MODE`: hace una syscall `modeswitch(MODE)`
- `demo`: hace `modeswitch(1)` y spawnea un deadpool en un proceso separado cada
   vez que se ejecuta
- `plottest X Y COLOR`: hace una syscall `plotpixel(X, Y, COLOR)`

Cuando se ejecuta `demo` el shell de qemu sigue funcionando, por ende
se puede ejecutar `demo` varias veces (*altamente recomendado*), o volver
al modo texto con `vgatest 0`. Cada vez que se ejecuta `demo` se crea un proceso
para animar a un nuevo deadpool.

*PD: Como mato a los deadpools? **Deadpool no puede morir**.*  
*PD2: Es divertido ver como el scheduler le da prioridad a los distintos deadpools*


# Parte 1

En esta parte usamos la información de la consigna para saber donde comenzar a
escribir la memoria para mostrar caracteres en pantalla (`0xB8000`).

Nos encontramos con varios problemas a la hora de hacer coincidir el manejo de
punteros de c con los requerimentos del CRTC. Estos problemas nos hicieron rever
el funcionamiento de los punteros, como castear valores y familiarizarnos un
poco más con los operadores bitwise.


# Parte 2

Al buscar como cambiar  de modo `03h` a `13h` los principales resultados
indicaban que había que setear el registro `ax` en `0x13` y luego realizar una
interrupción `10h`, sin embargo no logramos que esto funcione.

Reviendo el material de ayuda de la consigna encontramos, en el archivo
`modes.c`, que el cambio se realizaba usando funciones `outb` e `inb`.

Investigando más acerca de esto descubrimos que x86 por defecto tiene dos
instrucciones assembler, `in` y `out`, que funcionan como un store y load normal
con la única diferencia que las direcciones de "memoria" mapean a puertos I/O.

Por otro lado entendimos que qemu emula un monitor CRT (Cathode ray tube), que a
su vez tiene una placa controladora llamada CRTC (CRT Controller). La documentación
sobre como comunicarse con esta placa la encontramos en las ayudas del enunciado
(*"Puertos VGA"*). El procedimiento general es relativamente simple:

  1. Se escribe a un puerto índice, indicando que información se desea
     modificar/leer
  2. Luego se escribe, normalmente, en la dirección siguiente, los datos.

Luego de entender como funcionaba adaptamos la función `write_registers` a nuestro
código en `set_graphics_mode`, pasando por alto algunas medidas de seguridad que
no aplican a nuestro caso, ignorando modificaciones al cursor, etc.

La documentación a la que nos referimos para entender que hacia cada valor de
`write_registers` fue principalmente la de http://www.osdever.net/FreeVGA/vga.


# Parte 3

Para la parte 3 y la implementacion de la syscall tuvimos que buscar en todo el
código como estaban definidas las llamadas al sistema y entender cómo funcionaban
para poder implementar `modeswitch`.

Una de las cuestiones que más nos costó fue darnos cuenta cómo se leían los
argumentos del stack desde una syscall (`argint`, `argptr`, etc).

Para implementar `plotpixel` utilizamos una lógica similar a la de la parte 1
para escribir caracteres, solo que esta vez intentamos modularizar un poco más
el código. En la parte siguiente la adaptamos para poder mejorar la animación
que teníamos en mente.

Para testear que plotpixel funcionara correctamente creamos dos user programs,
`vgatest.c` y `plottest.c`, que nos permitían printear píxeles en pantalla.

Por otro lado, decidimos en esta parte modularizar todas estas funciones y las
anteriores a un nuevo archivo `vga.c` como indica uno de los puntos extras.
Decidimos también no hacer `vga.h` ya que existen funciones similares en xv6
y todas ellas están definidas en `defs.h`, por lo cual, terminamos por definirlas
ahí.


# Parte 4

Comenzamos probando los ejemplos dados en el enunciado para asegurarnos que
todas las funciones implementadas funcionaran correctamente.

Desde un comienzo teníamos pensado realizar algún tipo de animación, por lo
cual, comenzamos a pensar formas de mostrar sprites animados. Para esto
necesitábamos primero que nada de `delay`.

En `delay` nos encontramos con varios problemas ya que desde `vga.c` no hay una
forma simple de acceder a `sleep`, por ende, tuvimos que hacer uno propio que el
compilador no paraba de optimizar. Finalmente encontramos la keyword `volatile`
que solucionó nuestro problema.

Una vez solucionado `delay` comenzamos a implementar `animate`, que intercala
sprites de cierto tamaño en una posición dada.

Al comenzar a necesitar más colores, y luego de entender como
se implementaban (http://www.osdever.net/FreeVGA/vga/vgareg.htm,
  https://github.com/sam46/xv6/blob/master/vga.c) decidimos usar la paleta de
colores de material design (https://material.io/design/color/the-color-system.html),
que son justo 256. Por otro lado, hicimos un script en python `pixtohex.py` que
toma una imagen y la transforma a un array de índices de dicha paleta, encontrando
el color más cercano en la mismas.

Al comenzar a graficar nuestra animación nos dimos cuenta que trabajar con
`animate` a nivel de usuario era muy lento, por lo cual, tuvimos que implementarla
como una syscall. También implementamos una función `drawrect` como syscall para
limpiar el sprite anterior en la animación.

Para recuperar las fuentes tuvimos que entender un poco mejor cómo está
distribuida la memoria del VGA. Esta se divide en 4 planos/maps/secciones
de memoria, que pueden ser mapeadas a direcciones accesibles por el cpu y
pueden ser utilizadas de formas diferentes (*en particular vamos a usar
  read/write mode 0, las otras son variantes que no investigamos en profundidad
  que parecen aplicar transformaciones por hardware a los datos*).
Entonces, para leer y guardar las fuentes, necesitamos que el sequencer (`0x3C4`)
nos permita acceder al plano 2 (*donde estan las fuentes*) y queremos configurar
el Graphic Address Register (`0x3CE`) para que nos deje leer de forma continua
(*sin odd/even*). Además, hay que mapear los planos a memoria accesible por
el procesador (*a partir de `0xA0000`*).
Todo esto está implementado en `select_fontmem()` y `unselect_fontmem()`.
